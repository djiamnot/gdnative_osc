#ifndef OSCUTILS_H
#define OSCUTILS_H

#include <cstdlib>
#include <iostream>

#include <String.hpp>

namespace utils {
	std::string gdStringToString(godot::String s);
}  // end namespace utils

#endif
